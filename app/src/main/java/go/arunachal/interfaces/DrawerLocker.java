package go.arunachal.interfaces;

public interface DrawerLocker {
    public void setDrawerEnabled(boolean enabled);
}