package go.arunachal.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import go.arunachal.R;
import go.arunachal.helperclasses.Common;
import go.arunachal.helperclasses.Constants;
import go.arunachal.helperclasses.VolleySingleton;

public class EditPasswordActivity extends AppCompatActivity {
    Common common;
    RequestQueue requestQueue;
    EditText edt_old_Password, edtReEnter_newPassword, edt_new_password;
    Button btnSubmit;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_password);
        common = new Common(this);
        preferences = getSharedPreferences(Constants.preference, MODE_PRIVATE);
        editor = preferences.edit();
        requestQueue = VolleySingleton.getInstance(this).getRequestQueue();
        edt_old_Password = findViewById(R.id.edt_old_Password);
        edt_new_password = findViewById(R.id.edt_new_password);
        edtReEnter_newPassword = findViewById(R.id.edtReEnter_newPassword);
        btnSubmit = findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (common.isNetworkAvailable()) {
                    String newPass = edt_new_password.getText().toString().trim();
                    String newRe_Enter_Pass = edtReEnter_newPassword.getText().toString().trim();
                    String old_Pass = edt_old_Password.getText().toString().trim();
                    if (!old_Pass.isEmpty() && !newPass.isEmpty() && !newRe_Enter_Pass.isEmpty()) {
                       if(newPass.length()>=4) {
                           if (newPass.equals(newRe_Enter_Pass))
                               updateData();
                           else
                               common.showShortToast("Password do not match,please try again");
                       }else{
                           common.showShortToast("Password must be greater then 4 word.");
                       }
                    } else {
                        common.showShortToast("Fields  are empty");
                    }
                }else{
                    common.showShortToast("No internet.");
                }
            }
        });
    }


    private void updateData() {

        common.showSpinner(this);
        //http://goarunachal.com/changepassword.php
        String uri = Constants.BaseUrl + "changepassword.php";
        try {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, uri,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Toast.makeText(MainActivity.this,response,Toast.LENGTH_LONG).show();
                            String s = response.toString();
                            common.hideSpinner();
                            try {
                                JSONObject jsonObject=new JSONObject(response);
                                String status = jsonObject.optString("status");
                                String message = jsonObject.optString("message");

                                if (status.equalsIgnoreCase("1")) {
                                    JSONObject object=jsonObject.getJSONObject("UserDetail");
                                    common.showShortToast(message);
                                    /*editor.putString(Constants.Phone, object.optString("mobile"));
                                    editor.putString(Constants.email, object.optString("email"));
                                    editor.putString(Constants.FirstName, object.optString("name"));
                                    editor.putString(Constants.id, object.optString("id"));
                                    editor.putString(Constants.ProfilePic, object.optString("imgurl"));*/
                                    editor.putString(Constants.Password, object.optString("password"));
                                    editor.putBoolean(Constants.isLoggedIn, true);
                                    editor.commit();
                                    Intent returnIntent = new Intent();
                                    returnIntent.putExtra("result",edt_new_password.getText().toString().trim());
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();



                                } else {
                                    common.showShortToast(message);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                        }
                    }){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("oldpassword", edt_old_Password.getText().toString().trim());
                    params.put("newpass", edt_new_password.getText().toString().trim());
                    params.put("confirmnewpass", edtReEnter_newPassword.getText().toString().trim());
                    params.put("userid", common.getStringValue(Constants.id));
                    return params;
                }

            };

            try {
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);
            } catch (Exception e) {
                e.printStackTrace();
                common.hideSpinner();
            }
        } catch (Exception e) {
            e.printStackTrace();
            common.hideSpinner();

        }
    }

}
