package go.arunachal.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import go.arunachal.R;
import go.arunachal.helperclasses.Common;
import go.arunachal.helperclasses.Constants;
import go.arunachal.helperclasses.VolleySingleton;

public class SignUpActivity extends AppCompatActivity {

    EditText edtName, edtPassword,edtemail,edtPhone,edtConPassword;
    Button btnNext,btnLogin,btnSignUp;
    String name = "", password,email,pnoneNo;
    Common common;
    RequestQueue requestQueue;
    String emailPattern = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
            + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
            + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Typeface sfmedium,sfregular;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);
        common =Common.getNewInstance(this);
        edtName = findViewById(R.id.edtName);
        edtPassword = findViewById(R.id.edtPassword);
        edtConPassword = findViewById(R.id.edtConPassword);
        edtemail = findViewById(R.id.edtemail);
        edtPhone = findViewById(R.id.edtPhone);
        btnNext = findViewById(R.id.btnNext);
        btnLogin = findViewById(R.id.btnLogin);
        btnSignUp = findViewById(R.id.btnSignUp);
        requestQueue = VolleySingleton.getInstance(this).getRequestQueue();
        sfmedium = Typeface.createFromAsset(this.getAssets(), "sfmedium.ttf");
        sfregular = Typeface.createFromAsset(this.getAssets(), "sfregular.ttf");
        btnLogin.setTypeface(sfmedium);
        btnNext.setTypeface(sfmedium);

        edtName.setTypeface(sfregular);
        edtPassword.setTypeface(sfregular);
        edtConPassword.setTypeface(sfregular);
        edtPhone.setTypeface(sfregular);
        edtemail.setTypeface(sfregular);
        btnSignUp.setTypeface(sfregular);
        btnLogin.setPaintFlags(btnSignUp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        btnLogin.setText("LOGIN");

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = edtName.getText().toString().trim();
                password = edtPassword.getText().toString().trim();
                pnoneNo = edtPhone.getText().toString().trim();
                email = edtemail.getText().toString().trim();
                 preferences = getSharedPreferences(Constants.preference, MODE_PRIVATE);
                editor = preferences.edit();
                if (ValidateForm()) {
                    if (new Common(SignUpActivity.this).isNetworkAvailable()) {
                       /* Intent intent=new Intent(SignUpActivity.this,MainActivity.class);
                        startActivity(intent);
                        finishAffinity();*/
                       getData();
                    }else{
                        common.showShortToast("No internet connection");
                    }
                }
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    private void getData() {

        common.showSpinner(this);

        String uri = Constants.BaseUrl + "registration.php";
        try {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, uri,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                           // Toast.makeText(MainActivity.this,response,Toast.LENGTH_LONG).show();
                            String s = response.toString();
                            common.hideSpinner();
                            try {
                                JSONObject jsonObject=new JSONObject(response);
                                String status = jsonObject.optString("status");
                                String message = jsonObject.optString("message");

                                if (status.equalsIgnoreCase("1")) {
                                    common.showShortToast(message);
                                    JSONObject object=jsonObject.getJSONObject("UserDetail");
                                    editor.putString(Constants.Phone, pnoneNo);
                                    editor.putString(Constants.email, email);
                                    editor.putString(Constants.FirstName, name);
                                    editor.putString(Constants.Password, password);
                                    editor.putString(Constants.id, object.optString("id"));
                                    editor.putBoolean(Constants.isLoggedIn, true);
                                 //   editor.putString(Constants.ProfilePic, object.optString("imgurl"));

                                    editor.commit();
                                    Intent intent=new Intent(SignUpActivity.this,MainActivity.class);
                                    startActivity(intent);
                                    finishAffinity();

                                } else {
                                    common.showShortToast(message);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                           // Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                        }
                    }){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("password", password);
                    params.put("email", email);
                    params.put("mobile", pnoneNo);
                    params.put("name", name);
                    return params;
                }

            };

            try {
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);
            } catch (Exception e) {
                e.printStackTrace();
                common.hideSpinner();
            }
        } catch (Exception e) {
            e.printStackTrace();
            common.hideSpinner();

        }
    }

    public boolean ValidateForm() {
        if (name.isEmpty()) {
            edtName.setError("Please enter Name.");
            edtName.requestFocus();
            return false;
        }
        if (!edtemail.getText().toString().matches(emailPattern)) {
            edtemail.setError("Please enter valid email format.");
            edtemail.requestFocus();
            return false;
        }
        if (pnoneNo.isEmpty()) {
            edtPhone.setError("Please enter phoneNo");
            edtPhone.requestFocus();
            return false;
        }
        if  ((edtPhone.getText().toString().trim()).length() < 9) {
            edtPhone.setError(" Enter phoneNo is wrong");
            edtPhone.requestFocus();
            return false;
        }
        if (password.isEmpty()) {
            edtPassword.setError("Please enter password.");
            edtPassword.requestFocus();
            return false;
        }if (!password.equals(edtConPassword.getText().toString().trim())) {
            edtConPassword.setError("Password and Confirm password are not matched");
            edtConPassword.requestFocus();
            return false;
        }
        if ((edtPassword.getText().toString().trim()).length() < 3) {
            edtPassword.setError(" Password should be minimum five word.");
            edtPassword.requestFocus();
            return false;
        }
        return true;
    }

}


