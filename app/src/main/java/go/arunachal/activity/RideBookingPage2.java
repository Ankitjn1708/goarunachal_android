package go.arunachal.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import go.arunachal.R;
import go.arunachal.fragment.HomeFragment;
import go.arunachal.helperclasses.Common;
import go.arunachal.helperclasses.Constants;
import go.arunachal.helperclasses.VolleySingleton;
import go.arunachal.model.PackagesModel;

public class RideBookingPage2 extends AppCompatActivity {
    ImageView select1st, select1st2, select1st3;
    EditText edtName, edtPassword;
    Button btnNext, btnSignUp, btnlogin;
    String id = "", password;
    Common common;
    RequestQueue requestQueue;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Typeface sfmedium, sfregular;
    TextView origin, textViewtitle, txtDistance, txtdestination, txtReplain,
            txtTypePremiumValue, txtTypePremium, txtTypeStandardValue, txtTypeStandard, txtTypeEconomyValue, txtTypeEconomy;
    String originId, destId, originPlace, destinationPlace, distance,Time;
    ArrayList<PackagesModel> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_booking_page2);
        common = Common.getNewInstance(this);
        requestQueue = VolleySingleton.getInstance(this).getRequestQueue();
        preferences = getSharedPreferences(Constants.preference, MODE_PRIVATE);
        editor = preferences.edit();
        select1st2 = findViewById(R.id.select1st2);
        select1st = findViewById(R.id.select1st);
        select1st3 = findViewById(R.id.select1st3);

        origin = findViewById(R.id.origin);
        textViewtitle = findViewById(R.id.textViewtitle);
        txtDistance = findViewById(R.id.txtDistance);
        txtdestination = findViewById(R.id.txtdestination);
        txtReplain = findViewById(R.id.txtReplain);
        txtTypePremiumValue = findViewById(R.id.txtTypePremiumValue);
        txtTypePremium = findViewById(R.id.txtTypePremium);
        txtTypeStandardValue = findViewById(R.id.txtTypeStandardValue);
        txtTypeStandard = findViewById(R.id.txtTypeStandard);
        txtTypeEconomy = findViewById(R.id.txtTypeEconomy);
        txtTypeEconomyValue = findViewById(R.id.txtTypeEconomyValue);
        sfmedium = Typeface.createFromAsset(this.getAssets(), "sfmedium.ttf");
        sfregular = Typeface.createFromAsset(this.getAssets(), "sfregular.ttf");

        destId = getIntent().getStringExtra("destId");
        originId = getIntent().getStringExtra("originId");
        originPlace = getIntent().getStringExtra("originPlace");
        destinationPlace = getIntent().getStringExtra("destinationPlace");

        origin.setText(originPlace);
        txtdestination.setText(destinationPlace);

        origin.setTypeface(sfregular);
        txtdestination.setTypeface(sfregular);
        txtTypePremiumValue.setTypeface(sfregular);
        txtTypeStandardValue.setTypeface(sfregular);
        txtTypeEconomyValue.setTypeface(sfregular);
        txtDistance.setTypeface(sfregular);

        txtReplain.setTypeface(sfmedium);
        textViewtitle.setTypeface(sfmedium);
        txtTypePremium.setTypeface(sfmedium);
        txtTypeStandard.setTypeface(sfmedium);
        txtTypeEconomy.setTypeface(sfmedium);
        txtReplain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        /*Intent intent=new Intent(RideBookingPage2.this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);*/
                HomeFragment.edtOrigin.setText("");
                HomeFragment.edtDestination.setText("");
                HomeFragment.line.setVisibility(View.GONE);
                HomeFragment.dot1.setVisibility(View.VISIBLE);
                HomeFragment.dot2.setVisibility(View.VISIBLE);
                HomeFragment.destId="";
                HomeFragment.originId="";
                finish();
            }
        });
        select1st3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RideBookingPage2.this, RideBookingPage3.class);
                intent.putExtra("Type", "Premium");
                intent.putExtra("TotalCost", arrayList.get(2).getTotalCost());
                intent.putExtra("Distance", distance);
                intent.putExtra("DistanceTime", "35");
                intent.putExtra("originId", originId);
                intent.putExtra("destId", destId);
                intent.putExtra("destinationPlace", destinationPlace);
                intent.putExtra("originPlace", originPlace);
                intent.putExtra("PackageId", arrayList.get(2).getId());
                intent.putExtra("Time", Time);
                startActivity(intent);
            }
        });
        select1st2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RideBookingPage2.this, RideBookingPage3.class);
                intent.putExtra("Type", "Standard");
                intent.putExtra("TotalCost", arrayList.get(1).getTotalCost());
                intent.putExtra("Distance", distance);
                intent.putExtra("DistanceTime", "35");
                intent.putExtra("originId", originId);
                intent.putExtra("destId", destId);
                intent.putExtra("destinationPlace", destinationPlace);
                intent.putExtra("originPlace", originPlace);
                intent.putExtra("PackageId", arrayList.get(1).getId());
                intent.putExtra("Time", Time);
                startActivity(intent);
            }
        });
        select1st.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RideBookingPage2.this, RideBookingPage3.class);
                intent.putExtra("Type", "Economy");
                intent.putExtra("TotalCost", arrayList.get(0).getTotalCost());
                intent.putExtra("Distance", distance);
                intent.putExtra("DistanceTime", "35");
                intent.putExtra("originId", originId);
                intent.putExtra("destId", destId);
                intent.putExtra("destinationPlace", destinationPlace);
                intent.putExtra("originPlace", originPlace);
                intent.putExtra("PackageId", arrayList.get(0).getId());
                intent.putExtra("Time", Time);
                startActivity(intent);
            }
        });
        getData();
    }

    private void getData() {

        common.showSpinner(this);

        String uri = Constants.BaseUrl + "showpackages.php";
        try {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, uri,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Toast.makeText(MainActivity.this,response,Toast.LENGTH_LONG).show();
                            String s = response.toString();
                            common.hideSpinner();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.optString("status");
                                String message = jsonObject.optString("message");

                                if (status.equalsIgnoreCase("1")) {
                                   // common.showShortToast(message);
                                    parseData(jsonObject);

                                } else {
                                    common.showShortToast(message);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("fromid", originId);
                    params.put("toid", destId);

                    return params;
                }

            };

            try {
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);
            } catch (Exception e) {
                e.printStackTrace();
                common.hideSpinner();
            }
        } catch (Exception e) {
            e.printStackTrace();
            common.hideSpinner();

        }
    }

    private void parseData(JSONObject jsonObject) {
        try {
            distance = jsonObject.optString("Distance");
            Time = jsonObject.optString("Time");
            JSONArray jsonArray = jsonObject.getJSONArray("Package");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jObject1 = jsonArray.getJSONObject(i);
                PackagesModel packagesModel = new PackagesModel();
                packagesModel.setId(jObject1.optString("id"));
                packagesModel.setPackages(jObject1.optString("package"));
                packagesModel.setPriceperkm(jObject1.optString("priceperkm"));
                packagesModel.setTotalCost(jObject1.optString("TotalCost"));
                packagesModel.setTime(Time);

                arrayList.add(packagesModel);
            }
            txtDistance.setText(distance);
            if (arrayList.size() > 0) {
                txtTypeEconomyValue.setText("\u20B9 " + arrayList.get(0).getTotalCost());
                txtTypeStandardValue.setText("\u20B9 " + arrayList.get(1).getTotalCost());
                txtTypePremiumValue.setText("\u20B9 " + arrayList.get(2).getTotalCost());
            }
        } catch (Exception e) {

        }
    }
}
