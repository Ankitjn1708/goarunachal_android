package go.arunachal.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import go.arunachal.R;
import go.arunachal.helperclasses.Common;
import go.arunachal.helperclasses.Constants;
import go.arunachal.helperclasses.VolleySingleton;
import go.arunachal.model.PlaceModel;

public class PlacesActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    List<PlaceModel> placeList=new ArrayList<>();
    RequestQueue requestQueue;
    Common common;
    ImageView imgAdd;
    LinearLayoutManager linearLayoutManager;
    int cc = 1;
    EditText edtSearch;
    String edittextString = "";
    AdapterCampaignList mAdapter;
    boolean userScrolledFirstTym = false;
    private boolean loadingCompleted = false;
    RelativeLayout middle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);
        common = Common.getNewInstance(this);
        requestQueue = VolleySingleton.getInstance(this).getRequestQueue();
        recyclerView = findViewById(R.id.rvPlaceList);
        imgAdd = findViewById(R.id.imgAdd);
        edtSearch = findViewById(R.id.edtSearch);
        middle = findViewById(R.id.middle);
        /*AdapterCampaignList mAdapter = new AdapterCampaignList(this, placeList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);*/
        linearLayoutManager = new LinearLayoutManager(this);
        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             //   common.showSoftKeyboard(PlacesActivity.this);
               // getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

            }
        });
        middle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  common.showSoftKeyboard(PlacesActivity.this);
              //  getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

            }
        });

        // use a linear layout manager
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            int ydy = 0;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int offset = dy - ydy;
                ydy = dy;
                boolean shouldRefresh = (linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0)
                        && (recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING) && offset > 50;
                if (shouldRefresh) {
                    //swipeRefreshLayout.setRefreshing(true);
                    //Refresh to load data here.
                    // cc = cc + 1;
                    //  getData(String.valueOf(cc));
                    // getLocationList(String.valueOf(cc),"");
                    return;
                }
                boolean shouldPullUpRefresh = linearLayoutManager.findLastCompletelyVisibleItemPosition() == linearLayoutManager.getChildCount() - 1
                        && recyclerView.getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING && offset < -30;
                if (shouldPullUpRefresh && loadingCompleted && userScrolledFirstTym) {
                    //swipeRefreshLayout.setRefreshing(true);
                    //refresh to load data here.
                    cc = cc + 1;

                    loadingCompleted = false;
                    //  getData(String.valueOf(cc));
                    common.showSpinner(PlacesActivity.this);
                    getLocationList(String.valueOf(cc), "");
                    return;
                }
                //  swipeRefreshLayout.setRefreshing(false);
            }
        });
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                edittextString = s.toString();
                if (!edittextString.isEmpty()) {
                    recyclerView.setVisibility(View.VISIBLE);
                    requestQueue.cancelAll("tag_search");
                    if (common.isNetworkAvailable()) {
                        placeList.clear();
                        getLocationList("1", edittextString);
                    } else {
                        common.showShortToast("No internet");
                    }
                } else {

                    recyclerView.setVisibility(View.GONE);
                    if (mAdapter != null) {
                        mAdapter.notifyDataSetChanged();
                    }
                }

            }
        });

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PlacesActivity.this, AddNewPlace.class);
                startActivityForResult(intent, 100);
            }
        });
        common.showSpinner(this);
        getLocationList("1", "");
    }


    private void getLocationList(String pageNo, String text) {


//
        //http://goarunachal.com/locationlist.php?PageNo=1&Search=Vid
        String uri = Constants.BaseUrl + "locationlist.php?PageNo=" + pageNo + "&Search=" + text + "";
        try {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, uri, new JSONObject(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                //    String s = response.toString();
                                if (!response.optString("status").equalsIgnoreCase("0"))
                                parsedCatData(response);
                                else
                                    common.hideSpinner();
                            } catch (Exception e) {
                                e.printStackTrace();
                                common.hideSpinner();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            VolleyLog.d("error", error.getMessage());
                            common.hideSpinner();

                        }

                    }) {


                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    return headers;
                }
            };
            try {
                req.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(req);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            common.hideSpinner();
        }
    }

    void parsedCatData(JSONObject json) {

        try {
            //String  status=json.getString("");
            JSONArray array = json.getJSONArray("LocationList");
            //placeList=new ArrayList<>();

            for (int i = 0; i < array.length(); i++) {
                PlaceModel placeModel = new PlaceModel();
                JSONObject jsonObject = array.getJSONObject(i);
                String name = jsonObject.optString("name");
                String id = jsonObject.optString("id");
                String imageurl = jsonObject.optString("imageurl");
                String longitude = jsonObject.optString("longitude");
                String latitude = jsonObject.optString("latitude");
                placeModel.setId(id);
                placeModel.setPlaceName(name);
                placeModel.setPlaceImage(imageurl);
                placeModel.setLat(latitude);
                placeModel.setLongi(longitude);
                placeList.add(placeModel);

            }
            loadingCompleted = true;
            userScrolledFirstTym = true;

            if (placeList.size()== 10) {
                mAdapter = new AdapterCampaignList(this, placeList);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(mAdapter);
                recyclerView.scrollToPosition(placeList.size() - 10);

            }else{
                mAdapter.notifyDataSetChanged();
            }

        } catch (JSONException e) {
            e.printStackTrace();
            common.hideSpinner();
        }

        common.hideSpinner();
    //    common.hideKeyboard(this);

    }


    public class AdapterCampaignList extends RecyclerView.Adapter<AdapterCampaignList.MyViewHolder> {

        Context context;
        Common common;
        int width1, height1;
        private List<PlaceModel> campList;

        public AdapterCampaignList(Context context, List<PlaceModel> campList) {
            this.campList = campList;
            this.context = context;
            common = new Common(context);
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_place_list, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final String time, background_image, isCompleted, expirydate, system, CampStatus;

            WindowManager wm = (WindowManager) context.getSystemService(context.WINDOW_SERVICE);
            final DisplayMetrics displayMetrics = new DisplayMetrics();
            wm.getDefaultDisplay().getMetrics(displayMetrics);
            height1 = displayMetrics.heightPixels;
            width1 = displayMetrics.widthPixels;


            holder.placeimage.getLayoutParams().width = width1;
            holder.placeimage.getLayoutParams().height = (int) (width1 * .3);
            holder.placeName.setText(campList.get(position).getPlaceName());
            Picasso.with(context).load(campList.get(position).getPlaceImage()).into(holder.placeimage);
            holder.selectPlace.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result",campList.get(position).getPlaceName());
                    returnIntent.putExtra("placeId", campList.get(position).getId());
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                }
            });

        }

        @Override
        public int getItemCount() {
            return campList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView placeName;
            ImageView placeimage, selectPlace;


            public MyViewHolder(View view) {
                super(view);
                placeName = (TextView) view.findViewById(R.id.placeName);

                placeimage = (ImageView) view.findViewById(R.id.placeimage);
                selectPlace = (ImageView) view.findViewById(R.id.selectPlace);

            }
        }
    }
}
