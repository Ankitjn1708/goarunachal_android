package go.arunachal.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import go.arunachal.R;
import go.arunachal.helperclasses.Common;
import go.arunachal.helperclasses.Constants;
import go.arunachal.helperclasses.VolleySingleton;

public class LoginActivity extends AppCompatActivity {

    EditText edtName, edtPassword;
    Button btnNext,btnSignUp,btnlogin;
    String id = "", password;
    Common common;
    RequestQueue requestQueue;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Typeface sfmedium,sfregular;
    TextView txtForgotPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        common =Common.getNewInstance(this);
        requestQueue = VolleySingleton.getInstance(this).getRequestQueue();
        preferences = getSharedPreferences(Constants.preference, MODE_PRIVATE);
        editor = preferences.edit();
        edtName = (EditText) findViewById(R.id.edtName);
        edtPassword =  (EditText)findViewById(R.id.edtPassword);
        txtForgotPass =  (TextView) findViewById(R.id.txtForgotPass);
        btnNext = findViewById(R.id.btnNext);
        btnSignUp = findViewById(R.id.btnSignUp);
        btnlogin = findViewById(R.id.btnlogin);

        sfmedium = Typeface.createFromAsset(this.getAssets(), "sfmedium.ttf");
        sfregular = Typeface.createFromAsset(this.getAssets(), "sfregular.ttf");
        btnSignUp.setTypeface(sfmedium);
        txtForgotPass.setTypeface(sfmedium);
        btnNext.setTypeface(sfmedium);
        edtName.setTypeface(sfregular);
        edtPassword.setTypeface(sfregular);
        btnlogin.setTypeface(sfregular);
        btnSignUp.setPaintFlags(btnSignUp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtForgotPass.setPaintFlags(txtForgotPass.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        btnSignUp.setText("SIGNUP");

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id = edtName.getText().toString().trim();
                password = edtPassword.getText().toString().trim();
                if (ValidateForm()) {
                    if (new Common(LoginActivity.this).isNetworkAvailable()) {
                       /* Intent intent=new Intent(LoginActivity.this,MainActivity.class);
                        startActivity(intent);
                        finishAffinity();*/
                        getData();
                    }else{
                        common.showShortToast("No internet connection");
                    }
                }
            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this,SignUpActivity.class);
                startActivity(intent);
            }
        });
        txtForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this,ForgotPassordPage1.class);
                startActivity(intent);
            }
        });
    }
    private void getData() {

        common.showSpinner(this);

        String uri = Constants.BaseUrl + "login.php";
        try {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, uri,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Toast.makeText(MainActivity.this,response,Toast.LENGTH_LONG).show();
                            String s = response.toString();
                            common.hideSpinner();
                            try {
                                JSONObject jsonObject=new JSONObject(response);
                                String status = jsonObject.optString("status");
                                String message = jsonObject.optString("message");

                                if (status.equalsIgnoreCase("1")) {
                                    common.showShortToast(message);

                                    JSONObject object=jsonObject.getJSONObject("UserDetail");

                                    editor.putString(Constants.Phone, object.optString("mobile"));
                                    editor.putString(Constants.email, object.optString("email"));
                                    editor.putString(Constants.FirstName, object.optString("name"));
                                    editor.putString(Constants.Password, object.optString("password"));
                                    editor.putString(Constants.id, object.optString("id"));
                                    editor.putString(Constants.ProfilePic, object.optString("imgurl"));
                                    editor.putBoolean(Constants.isLoggedIn, true);
                                    editor.commit();
                                    Intent intent=new Intent(LoginActivity.this,MainActivity.class);
                                    startActivity(intent);
                                    finishAffinity();

                                } else {
                                    common.showShortToast(message);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                        }
                    }){
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("password", password);
                    params.put("emailmobile", id);

                    return params;
                }

            };

            try {
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);
            } catch (Exception e) {
                e.printStackTrace();
                common.hideSpinner();
            }
        } catch (Exception e) {
            e.printStackTrace();
            common.hideSpinner();

        }
    }
    public boolean ValidateForm() {
        if (id.isEmpty()) {
            edtName.setError("Please enter Id.");
            edtName.requestFocus();
            return false;
        }
        /*if (!txtEmailId.getText().toString().matches(emailPattern)) {
            txtEmailId.setError("Please enter valid email format.");
            txtEmailId.requestFocus();
            return false;
        }*/
        if (password.isEmpty()) {
            edtPassword.setError("Please enter password.");
            edtPassword.requestFocus();
            return false;
        }
        if ((edtPassword.getText().toString().trim()).length() < 3) {
            edtPassword.setError(" Password should be minimum five word.");
            edtPassword.requestFocus();
            return false;
        }
        return true;
    }

}


