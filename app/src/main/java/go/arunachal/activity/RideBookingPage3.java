package go.arunachal.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import go.arunachal.R;
import go.arunachal.fragment.HomeFragment;
import go.arunachal.helperclasses.Common;
import go.arunachal.helperclasses.Constants;
import go.arunachal.helperclasses.VolleySingleton;

public class RideBookingPage3 extends AppCompatActivity {
    String bookingId="",driverid="",originId, destId,time, originPlace, destinationPlace, distance, Type, TotalCost, DistanceTime, userId, packageId = "";
    TextView origin, textViewtitle, txtDistance, txtdestination, txtConfirmRide,
            txtTotalTimeValue, txtTymEsti, txtCost, txtFareCost, select1stType, youChoose;
    Common common;
    RequestQueue requestQueue;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Typeface sfmedium, sfregular;
    boolean isLoggedIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_booking_page3);
        preferences = getSharedPreferences(Constants.preference, MODE_PRIVATE);
        editor = preferences.edit();
        requestQueue = VolleySingleton.getInstance(this).getRequestQueue();
        common = new Common(this);
        userId = common.getStringValue(Constants.id);
        origin = findViewById(R.id.origin);
        textViewtitle = findViewById(R.id.textViewtitle);
        txtDistance = findViewById(R.id.txtDistance);
        txtdestination = findViewById(R.id.txtdestination);
        txtConfirmRide = findViewById(R.id.txtConfirmRide);
        txtTotalTimeValue = findViewById(R.id.txtTotalTimeValue);
        txtTymEsti = findViewById(R.id.txtTymEsti);
        txtCost = findViewById(R.id.txtCost);
        txtFareCost = findViewById(R.id.txtFareCost);
        youChoose = findViewById(R.id.youChoose);
        select1stType = findViewById(R.id.select1stType);
        sfmedium = Typeface.createFromAsset(this.getAssets(), "sfmedium.ttf");
        sfregular = Typeface.createFromAsset(this.getAssets(), "sfregular.ttf");


        destId = getIntent().getStringExtra("destId");
        originId = getIntent().getStringExtra("originId");
        originPlace = getIntent().getStringExtra("originPlace");
        destinationPlace = getIntent().getStringExtra("destinationPlace");
        distance = getIntent().getStringExtra("Distance");
        Type = getIntent().getStringExtra("Type");
        TotalCost = getIntent().getStringExtra("TotalCost");
        DistanceTime = getIntent().getStringExtra("DistanceTime");
        packageId = getIntent().getStringExtra("PackageId");
        time = getIntent().getStringExtra("Time");

        Drawable dr = getResources().getDrawable(R.drawable.watch_icon);
        Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
        Drawable d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 35, 40, true));
        txtTotalTimeValue.setCompoundDrawablesWithIntrinsicBounds(d, null, null, null);
        origin.setText(originPlace);
        txtdestination.setText(destinationPlace);

        txtCost.setText("\u20B9 " + TotalCost);
        txtDistance.setText(distance);
        txtTotalTimeValue.setText(" " + time );
        select1stType.setText(Type);
        txtdestination.setText(destinationPlace);

        origin.setTypeface(sfregular);
        txtdestination.setTypeface(sfregular);
        txtTotalTimeValue.setTypeface(sfregular);
        txtCost.setTypeface(sfregular);
        select1stType.setTypeface(sfmedium);
        txtDistance.setTypeface(sfregular);

        txtConfirmRide.setTypeface(sfmedium);
        textViewtitle.setTypeface(sfmedium);
        txtCost.setTypeface(sfmedium);
        txtFareCost.setTypeface(sfmedium);
        txtTymEsti.setTypeface(sfmedium);
        // txtTypeEconomy.setTypeface(sfmedium);
        txtConfirmRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (common.isNetworkAvailable()) {

                    isLoggedIn = preferences.getBoolean(Constants.isLoggedIn, false);

                    if (isLoggedIn) {
                        getData();
                    } else {
                        common.showShortToast("Please Login/signUp first.");
                        new AlertDialog.Builder(RideBookingPage3.this)
                                .setMessage("Please Login/signUp first.")
                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(RideBookingPage3.this, LoginActivity.class);
                                        startActivity(intent);
                                        finishAffinity();
                                    }
                                }).show();
                    }


                } else {
                    common.showShortToast("No internet");
                }
            }
        });
    }

    private void getData() {

        common.showSpinner(this);

        String uri = Constants.BaseUrl + "booking.php";
        try {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, uri,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Toast.makeText(MainActivity.this,response,Toast.LENGTH_LONG).show();
                            String s = response.toString();
                            common.hideSpinner();
                            try {
                                JSONObject jsonObject2 = new JSONObject(response);
                                String status = jsonObject2.optString("status");
                                String message = jsonObject2.optString("message");

                                if (status.equalsIgnoreCase("1")) {
                                    common.showShortToast(message);
                                    JSONArray finalBookingjsonArray = jsonObject2.getJSONArray("FinalBookingInfo");
                                    JSONObject jsonObject = finalBookingjsonArray.optJSONObject(0);
                                    JSONObject jsonFromLocation = jsonObject.optJSONObject("BookingFromLocation");
                                    JSONObject jsonToLocation = jsonObject.optJSONObject("BookingToLocation");
                                    JSONObject jsonBookingInfo = jsonObject.optJSONObject("BookingInfo");
                                    JSONObject jsonUserDetail = jsonObject.optJSONObject("UserDetail");
                                    String fromLocation = jsonFromLocation.optString("name");
                                    String toLocation = jsonToLocation.optString("name");
                                    String userName = jsonUserDetail.optString("name");
                                    String userPhone = jsonUserDetail.optString("mobile");
                                    String userPicUrl = jsonUserDetail.optString("imgurl");
                                    driverid = jsonBookingInfo.optString("driverid");
                                    bookingId = jsonBookingInfo.optString("id");
                                    common.setStringValue("driverid",driverid);
                                    common.setStringValue("bookingId",bookingId);
                                    Intent intent = new Intent(RideBookingPage3.this, MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finishAffinity();
                                  //  parseData(jsonObject);
                                    HomeFragment.destId="";
                                    HomeFragment.originId="";

                                } else {
                                    common.showShortToast(message);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("fromid", destId);
                    params.put("toid", originId);
                    params.put("packageid", packageId);
                    params.put("userid", userId);
                    params.put("totalcost", TotalCost);
                    return params;
                }

            };

            try {
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);
            } catch (Exception e) {
                e.printStackTrace();
                common.hideSpinner();
            }
        } catch (Exception e) {
            e.printStackTrace();
            common.hideSpinner();

        }
    }

    private void parseData(JSONObject jsonObject) {
        try {
            distance = jsonObject.optString("Distance");
            JSONArray jsonArray = jsonObject.getJSONArray("Package");

        } catch (Exception e) {

        }
    }
}
