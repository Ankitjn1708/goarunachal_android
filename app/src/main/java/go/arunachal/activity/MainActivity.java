package go.arunachal.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import go.arunachal.R;
import go.arunachal.adapter.NavDrawerListAdapter;
import go.arunachal.fragment.AboutUsFragment;
import go.arunachal.fragment.BaseFragment;
import go.arunachal.fragment.ContectUpFragment;
import go.arunachal.fragment.HomeFragment;
import go.arunachal.fragment.MyProfileFragment;
import go.arunachal.fragment.TOSfragment;
import go.arunachal.helperclasses.Common;
import go.arunachal.helperclasses.Constants;
import go.arunachal.interfaces.DrawerLocker;
import go.arunachal.interfaces.ToolbarListner;
import go.arunachal.model.NavDrawerItem;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ToolbarListner, DrawerLocker {

    public static ArrayList<NavDrawerItem> navDrawerItems;
    static DrawerLayout drawer;
    public android.support.v4.app.FragmentManager fragmentManager;
    Toolbar mToolbar;
    ListView listView;
    Fragment fragment;
    ActionBarDrawerToggle toggle;
    android.support.v4.app.FragmentTransaction fragmentTransaction;
    NavDrawerListAdapter navDrawerListAdapter;
    boolean isLoggedIn;
    Common common;
    SharedPreferences preferences;
    int[] myImageList = new int[]{R.drawable.home, R.drawable.my_profile,R.drawable.add_image, R.drawable.login,
            R.drawable.ic_about_app, R.drawable.t_condition, R.drawable.contact_us, R.drawable.share};
    TextView headeName, headerEmail;
    Typeface sfmedium, sfregular;
    private String[] navMenuTitles;
    boolean doubleBackToExitPressedOnce = false;

    public static boolean setListViewHeightBasedOnItems(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {
            int numberOfItems = listAdapter.getCount();
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight + 30;
            listView.setLayoutParams(params);
            listView.requestLayout();
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list_slidermenu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sfmedium = Typeface.createFromAsset(this.getAssets(), "sfmedium.ttf");
        sfregular = Typeface.createFromAsset(this.getAssets(), "sfregular.ttf");

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        common = Common.getNewInstance(this);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
        //   isLoggedIn = common.getBooleanValue(Constants.isLoggedIn);

        navDrawerItems = new ArrayList<NavDrawerItem>();
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], myImageList[0]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], myImageList[1]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], myImageList[2]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], myImageList[3]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], myImageList[4]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], myImageList[5]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], myImageList[6]));
        preferences = getSharedPreferences(Constants.preference, MODE_PRIVATE);
        isLoggedIn = preferences.getBoolean(Constants.isLoggedIn, false);


        if (isLoggedIn) {
            navDrawerItems.remove(3);
            navDrawerItems.add(3, new NavDrawerItem(navMenuTitles[8], myImageList[3]));
        } else {
            navDrawerItems.remove(3);
            navDrawerItems.add(3, new NavDrawerItem(navMenuTitles[3], myImageList[3]));
        }

        navDrawerListAdapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems);
        listView.setAdapter(navDrawerListAdapter); ///////////////////////////////////////
        setListViewHeightBasedOnItems(listView);

        NavigationView naviView = (NavigationView) findViewById(R.id.nav_view);
        naviView.setNavigationItemSelectedListener(this);


        View header = navigationView.getHeaderView(0);


        headeName = (TextView) header.findViewById(R.id.headeName);
        headerEmail = (TextView) header.findViewById(R.id.headerEmail);
      ImageView imageView= header.findViewById(R.id.imageView);
        // appVersion = (TextView) header.findViewById(R.id.header3);
        String name = common.getStringValue(Constants.FirstName);
        if (isLoggedIn) {
            headeName.setText(common.getStringValue(Constants.FirstName));
            headerEmail.setText(common.getStringValue(Constants.Phone));
        } else {
          //  headeName.setText("Arunachal");
          //  headerEmail.setText("Go.arunachal@gmail.com");
            imageView.setVisibility(View.GONE);
            headeName.setVisibility(View.GONE);
            headerEmail.setVisibility(View.GONE);
        }
        if (common.isNetworkAvailable()) {
            // profilePic
            if(!common.getStringValue(Constants.ProfilePic).trim().isEmpty()&&!common.getStringValue(Constants.ProfilePic).equalsIgnoreCase("null"))
                try {
                    Picasso.with(this).load(common.getStringValue(Constants.ProfilePic)).placeholder(R.drawable.galleryplacholder).into(imageView);
                }catch (Exception ex){

                }
        }
        headerEmail.setTypeface(sfmedium);
        headeName.setTypeface(sfmedium);
        fragment = new HomeFragment();
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction tx1 = fragmentManager.beginTransaction();
        tx1.replace(R.id.container_body, fragment);
        tx1.commit();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(navDrawerItems.get(position).title);
            }
        });
    }

    @Override
    public void onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
*/
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

       /* if (id == R.id.nav_camera) {
            // Handle the camera action
            Intent inten= new Intent(MainActivity.this,LoginActivity.class);
            startActivity(inten);
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {
            Intent inten= new Intent(MainActivity.this,LoginActivity.class);
            startActivity(inten);
        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void selectItem(String title) {

        //   mApprovalStatus = comonObj.getStringValue(Constants.approvalStatus);
        closeDrawer();
        switch (title) {

            case "Home": {
                fragment = new HomeFragment();
                fragmentManager = getSupportFragmentManager();
                FragmentTransaction tx1 = fragmentManager.beginTransaction();
                tx1.replace(R.id.container_body, fragment);
                tx1.commit();
               /* if (isLoggedIn && mApprovalStatus.equalsIgnoreCase("Y") && comonObj.getStringValue("ID").equalsIgnoreCase(comonObj.getStringValue(Constants.userRwa))) {
                    fragment = new HomePageOfSociety();
                    fragmentManager = getSupportFragmentManager();
                    FragmentTransaction tx1 = fragmentManager.beginTransaction();
                    tx1.replace(R.id.container_body, fragment);
                    tx1.commit();
                    relativeLayoutHeading.setVisibility(View.GONE);
                    downarrow.setVisibility(View.GONE);
                } else {
                    fragment = new HomePageLogOutFragment();
                    fragmentManager = getSupportFragmentManager();
                    FragmentTransaction tx1 = fragmentManager.beginTransaction();
                    tx1.replace(R.id.container_body, fragment);
                    tx1.addToBackStack("Home");
                    tx1.commit();
                  //  relativeLayoutHeading.setVisibility(View.GONE);
                    downarrow.setVisibility(View.GONE);
                }*/
                break;
            }


            case "My Profile": {


                if (isLoggedIn) {
                    Intent intent = new Intent(this, MyProfileFragment.class);
                    startActivity(intent);
                } else {
                   common.showShortToast("Please Login First.");
                }

                break;
            }

            case "App Feeds": {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
               /* if (isLoggedIn && mApprovalStatus.equalsIgnoreCase("Y")) {
                    Intent intent = new Intent(MainHomePageActivity.this, NewsFeedNotificationActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("fragmnet", "listNotificationNFeed");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    AlertDialogLogin dialog = new AlertDialogLogin();
                    dialog.setRetainInstance(true);
                    dialog.show(getSupportFragmentManager(), "tag_alert_login");
                }
*/
                break;
            }
            case "Add Your Location": {

                Intent intent = new Intent(this, AddNewPlace.class);
                startActivity(intent);
          //      finish();
                break;
            }
            case "Login/Signup": {

                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            }

            case "Logout": {
                new AlertDialog.Builder(this)
                        .setTitle("Logout")
                        .setMessage("Are you sure you want to logout?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                preferences = MainActivity.this.getSharedPreferences(Constants.preference, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putBoolean(Constants.isLoggedIn, false);

                                editor.remove(Constants.id);
                                editor.remove(Constants.Phone);
                                editor.remove(Constants.email);
                                editor.remove(Constants.Password);
                                editor.remove(Constants.FirstName);
                                editor.remove(Constants.ProfilePic);
                                editor.remove("bookingId");
                               /* editor.remove(Constants.MiddleName);
                                editor.remove(Constants.LastName);
                                editor.remove(Constants.ProfilePic);
                                editor.remove(Constants.Gender);
                                editor.remove(Constants.Occupation);
                                editor.remove(Constants.AddedOn);
                                editor.remove(Constants.isActive);
                                editor.remove(Constants.userRwa);
                                editor.remove(Constants.ResidentRWAID);
                                editor.remove(Constants.flatId);
                                editor.remove(Constants.flatNumber);
                                editor.remove(Constants.ResidentRWAID);
                                editor.remove(Constants.userRwaName);
                                editor.remove(Constants.isPhonePublic);
                                editor.remove(Constants.aboutMe);
                                editor.remove(Constants.VehicleList);
                                editor.remove(Constants.ParkingList);
                                editor.remove(Constants.notificationCount);
                                editor.remove(Constants.pollCount);
                                editor.remove(Constants.complaintCount);
                                editor.remove(Constants.classifiedCount);
                                editor.remove(Constants.calloutCount);
                                editor.remove(Constants.galleryCount);
                                editor.remove(Constants.neighborsCount);
                                editor.remove(Constants.helperCount);
                                editor.remove(Constants.notificationLastSeenTime);
                                editor.remove(Constants.pollLastSeenTime);
                                editor.remove(Constants.complaintLastSeenTime);
                                editor.remove(Constants.classifiedLastSeenTime);
                                editor.remove(Constants.calloutLastSeenTime);
                                editor.remove(Constants.galleryLastSeenTime);
                                editor.remove(Constants.neighborsLastSeenTime);
                                editor.remove(Constants.helperLastSeenTime);
                                editor.remove("Count");
                                editor.remove("token");*/
                               /* token = "";
                                callApi();*/
                                boolean b = editor.commit();
                                if (b) {
                                    navDrawerItems.remove(2);
                                    navDrawerItems.add(2, new NavDrawerItem(navMenuTitles[2], myImageList[2]));
                                    // navDrawerListAdapter.notifyDataSetChanged();
                                }
                                /*navDrawerListAdapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems);
                                listView.setAdapter(navDrawerListAdapter); ///////////////////////////////////////
                                setListViewHeightBasedOnItems(listView);*/
                                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                                isLoggedIn = false;
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                break;
            }

            case "About App": {
                Intent intent = new Intent(this, AboutUsFragment.class);
                startActivity(intent);
                break;

            }
            case "Terms & Conditions": {
               /* Intent intent = new Intent(this, TermsANDCondition.class);
                startActivity(intent);*/
                Intent intent = new Intent(this, TOSfragment.class);
                startActivity(intent);

                break;
            }
            case "Contact Us": {
                /*Intent intent = new Intent(this, ContactUs.class);
                startActivity(intent);*/
                Intent intent = new Intent(this, ContectUpFragment.class);
                startActivity(intent);
                break;
            }

            case "Share App": {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Download #mynukad app from apple or google store,please click this link on mobile device: http://mynukad.com/mynukaddownload.html");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);

                break;
            }


        }

    }

    public void openDrawer() {
        drawer.openDrawer(Gravity.RIGHT);
    }

    public void closeDrawer() {
        drawer.closeDrawer(Gravity.LEFT);
    }

    @Override
    public void onButtonClick(Fragment newfragment, Boolean isCommingBack) {

        Common.hideSoftKeyboard(this);
        fragment = (BaseFragment) newfragment;
        fragmentManager = getSupportFragmentManager();
        if (isCommingBack) {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);

            try {
                fragmentTransaction.commit();
            } catch (IllegalStateException ignored) {
            }
        } else {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit,
                    R.anim.slide_in_left, R.anim.slide_in_right);
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(fragment.getClass().getName());

            try {
                fragmentTransaction.commit();
            } catch (IllegalStateException ignored) {
            }

        }
    }

    @Override
    public void onButtonClickNoBack(Fragment newfragment) {
        fragment = (BaseFragment) newfragment;
        fragmentManager = getSupportFragmentManager();
        fragment = (BaseFragment) newfragment;
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit,
                R.anim.slide_in_left, R.anim.slide_in_right);
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void setDrawerEnabled(boolean enabled) {
        int lockMode = enabled ? DrawerLayout.LOCK_MODE_UNLOCKED :
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
        //   drawer.setDrawerLockMode(lockMode);
        //  toggle.setDrawerIndicatorEnabled(enabled);
    }


}
