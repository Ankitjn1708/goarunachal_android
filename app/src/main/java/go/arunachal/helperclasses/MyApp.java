package go.arunachal.helperclasses;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import com.instabug.library.Instabug;
import com.instabug.library.invocation.InstabugInvocationEvent;

public class MyApp extends MultiDexApplication {
    private static MyApp instance;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static MyApp getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
       // Fabric.with(this, new Crashlytics());
      //  new Instabug.Builder(this, "APP_TOKEN").build();
        new Instabug.Builder(this, "9ad3843282f9f9137ee9be7949194b3b")
                .setInvocationEvent(InstabugInvocationEvent.SHAKE).setShakingThreshold(500)
                .build();

    }

}