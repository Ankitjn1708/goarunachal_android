package go.arunachal.helperclasses;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

//import com.theartofdev.edmodo.cropper.CropImage;
//import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import go.arunachal.R;


public class Common {
    public static boolean debug = true;
    private static Common mInstance = null;
    public SharedPreferences pref;
    SharedPreferences.Editor editor;
    int PRIVATE_MODE = 0;
    Dialog loadDialog;
    private Context mcontext;

    public Common(Context ctx) {
        this.mcontext = ctx;
        pref = mcontext.getSharedPreferences(Constants.preference, PRIVATE_MODE);
        editor = pref.edit();
    }

    public static synchronized Common getNewInstance(Context ctx) {
        mInstance = new Common(ctx);
        return mInstance;
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);

            inputMethodManager.hideSoftInputFromWindow(activity
                    .getCurrentFocus().getWindowToken(), 0);

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static float getDistance(double startLati, double startLongi,
                                    double goalLati, double goalLongi) {
        Location locationA = new Location("point A");
        locationA.setLatitude(startLati);
        locationA.setLongitude(startLongi);
        Location locationB = new Location("point B");
        locationB.setLatitude(goalLati);
        locationB.setLongitude(goalLongi);
        float distance = (float) (locationA.distanceTo(locationB) * 0.000621371);
        return distance;
    }

    public void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);

    }

    public void showSoftKeyboard(Context ctx) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mcontext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();

        return activeNetworkInfo != null;
    }

    public void showShortToast(String output) {
        Toast.makeText(mcontext, output, Toast.LENGTH_SHORT).show();
    }

    public void showLongToast(String output) {
        Toast.makeText(mcontext, output, Toast.LENGTH_LONG).show();
    }

    public boolean getBooleanValue(String key) {
        return pref.getBoolean(key, false);
    }

    public void removeKey(String key) {
        editor.remove(key);
        editor.commit();
    }

    public String getStringValue(String key) {
        String str = pref.getString(key, " ");
        if (str.equalsIgnoreCase("null")) {
            str = " ";
        }
        return str;
    }

    public String getStringValueForFirebase(String key) {
        String str = pref.getString(key, "0");
        if (str.equalsIgnoreCase("null")) {
            str = "0";
        }
        return str;
    }

    public void setStringValue(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public void setIntValue(String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }


    public int getIntValue(String key) {
        return pref.getInt(key, -1);
    }


    public String convertFromUnix1(String unix_time) throws NullPointerException, IllegalArgumentException {
        long time = Long.valueOf(unix_time);
        String result = "";
        Date date = null;
        if (unix_time.length() > 10) {
            date = new Date(time);
        } else {
            date = new Date(time * 1000L);
        }
        SimpleDateFormat format = new SimpleDateFormat("d MMM, hh:mm a");
        format.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
        result = format.format(date);
        Log.d("date", format.format(date));

        return result;
    }


    public String convertFromUnix2(String unix_time) throws NullPointerException, IllegalArgumentException {
        long time = Long.valueOf(unix_time);
        String result = "";
        Date date = null;
        if (unix_time.length() > 10) {
            date = new Date(time);
        } else {
            date = new Date(time * 1000L);
        }
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        format.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
        result = format.format(date);
        Log.d("date", format.format(date));

        return result;
    }


    public String convertFromUnix3(String unix_time) throws NullPointerException, IllegalArgumentException {
        long time = Long.valueOf(unix_time);
        String result = "";
        Date date = null;
        if (unix_time.length() > 10) {
            date = new Date(time);
        } else {
            date = new Date(time * 1000L);
        }
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        format.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
        result = format.format(date);
        Log.d("date", format.format(date));

        return result;
    }

    public String convertFromUnix4(String unix_time) throws NullPointerException, IllegalArgumentException {
        long time = Long.valueOf(unix_time);
        String result = "";
        Date date = null;
        if (unix_time.length() > 10) {
            date = new Date(time);
        } else {
            date = new Date(time * 1000L);
        }
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a, d MMM, yy");
        format.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
        result = format.format(date);
        Log.d("date", format.format(date));

        return result;
    }


    public long getUnixTime() {
        long unixTime = System.currentTimeMillis() / 1000L;
        return unixTime;
    }

    public String funConvertBase64ToString(String base64String) {
        String base64URLString = null;
        try {
            base64URLString = URLDecoder.decode(base64String, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        byte[] data = Base64.decode(base64URLString, Base64.NO_WRAP);
        String text = null;
        try {
            text = new String(data, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }

    //only use for post data
    public String funConvertBase64ToPostString(String base64String) {

        byte[] data = Base64.decode(base64String, Base64.NO_WRAP);
        String text = null;
        try {
            text = new String(data, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }

    public String StringToBase64StringConvertion(String text) {
        String base64Result = "";
        String base64URLResult = "";
        if (text != null) {
            byte[] data = new byte[0];
            try {
                data = text.getBytes("UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            base64Result = Base64.encodeToString(data, Base64.NO_WRAP);
            try {
                base64URLResult = URLEncoder.encode(base64Result, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return base64URLResult;
    }

    //only use for post data
    public String StringToBase64POSTStringConvertion(String text) {
        String base64Result = "";
        if (text != null) {
            byte[] data = new byte[0];
            try {
                data = text.getBytes("UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            base64Result = Base64.encodeToString(data, Base64.NO_WRAP);

        }
        return base64Result;
    }

    public void showSpinner(Context context) {
        Log.e("BaseActivity", "showSpinner");
        if (loadDialog != null) {
            if (loadDialog.isShowing())
                loadDialog.dismiss();
        }
        loadDialog = new Dialog(context, R.style.TransparentDialogTheme);
        loadDialog.setContentView(R.layout.spinner_rotate);
        loadDialog.setCanceledOnTouchOutside(false);

        ImageView ivLoader = (ImageView) loadDialog.findViewById(R.id.ivSpinner);

        Animation animRotate = AnimationUtils.loadAnimation(context, R.anim.rotate);
        ivLoader.startAnimation(animRotate);
        loadDialog.show();
    }

    public void hideSpinner() {
        if (loadDialog != null && loadDialog.isShowing())
            loadDialog.dismiss();

    }

  /*  public void getImage_Activity_40_60(Activity context) {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(100, 40)
                .setAllowFlipping(false)
                .start(context);

    }*/
}
