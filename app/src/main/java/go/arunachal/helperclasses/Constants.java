package go.arunachal.helperclasses;

import org.json.JSONObject;

import java.util.HashSet;


public class Constants {

    public static String AWSLINK = "https://s3.ap-south-1.amazonaws.com/goarunachalnew/";
    public static final String COGNITO_POOL_ID = "ap-south-1:8ffbaef5-143f-4fa3-bfa7-d15c1a6cd22b";

    public static final String BUCKET_NAME = "goarunachalnew";
    public static final String BUCKET_NAME_GALLERY = "/gallery";
    public static String preference = "SHARED_PREF";
    public static String linkOne = "";
    public static String linkProfile = "";

    //user specific data
    public static String isLoggedIn = "isLoggedIn";

    public static String id = "userid";
    public static String Phone = "Phone";
    public static String email = "email";
    public static String Password = "Password";
    public static String FirstName = "firstName";
    public static String MiddleName = "MiddleName";
    public static String LastName = "LastName";
    public static String ProfilePic = "ProfilePic";
    public static String Gender = "Gender";
    public static String Occupation = "Occupation";
    public static String AddedOn = "AddedOn";
    public static String isActive = "isActive";
    public static String approvalStatus = "ApprovalStatus";



    //final public static String BaseUrl = "http://mynukad.com/nukkad_dev_api/nukadmanager.php/nukad/";//development
   // final public static String BaseUrl = " http://typingwork.co/testdb/";//production
    final public static String BaseUrl = "http://goarunachal.com/";//production
    //final public static String BaseUrl = "http://mynukad.com/NukkadStg/nukadmanager.php/nukad/";// staging




}
