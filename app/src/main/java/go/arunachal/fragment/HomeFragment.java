package go.arunachal.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Transformers.BaseTransformer;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import go.arunachal.R;
import go.arunachal.activity.PlacesActivity;
import go.arunachal.activity.RideBookingPage2;
import go.arunachal.helperclasses.Common;
import go.arunachal.helperclasses.Constants;
import go.arunachal.helperclasses.VolleySingleton;
import go.arunachal.model.BannerData;

import static android.content.Context.MODE_PRIVATE;


public class HomeFragment extends BaseFragment implements BaseSliderView.OnSliderClickListener, SwipeRefreshLayout.OnRefreshListener {

    public static ImageView cancle, save, dot1, dot2;
    public static TextView line;
    public static EditText edtOrigin, edtDestination;
    public static String originId = "", destId = "";
    View view;
    Common common;
    RequestQueue requestQueue;
    Typeface worksans_regular, ubuntuB;
    EditText oldpassword, newpassword, retypepassword;
    ImageView imgDreiverProPic, imgdriverCall;
    TextView textViewtitle, txtDriverStatus, txtVechileName, txtVechileNo, txtDriverName, txtDriverCancel, txtDriverphoneNo;
    boolean origin, dest;
    ArrayList<BannerData> listBanner = new ArrayList<>();
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    boolean isLoggedIn;
    Typeface sfmedium, sfregular;
    RelativeLayout rlStartlayout;
    SliderLayout mDemoSlider;
    String originPlace, destinationPlace, driverNo = "",carname="",carno="",driverid="";
    SwipeRefreshLayout setOnRefreshListener;
    String bookingId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.home_fragment, container, false);
        //((DrawerLocker) getActivity()).setDrawerEnabled(false);
        common = Common.getNewInstance(getActivity());
        requestQueue = VolleySingleton.getInstance(getActivity()).getRequestQueue();
        edtOrigin = view.findViewById(R.id.edtOrigin);
        edtDestination = view.findViewById(R.id.edtDestination);
        txtDriverStatus = view.findViewById(R.id.txtDriverStatus);
        txtVechileName = view.findViewById(R.id.txtVechileName);
        txtVechileNo = view.findViewById(R.id.txtVechileNo);
        txtDriverName = view.findViewById(R.id.txtDriverName);
        rlStartlayout = view.findViewById(R.id.rlStartlayout);
        imgDreiverProPic = view.findViewById(R.id.imgDreiverProPic);
        txtDriverCancel = view.findViewById(R.id.txtDriverCancel);
        txtDriverphoneNo = view.findViewById(R.id.txtDriverphoneNo);
        imgdriverCall = view.findViewById(R.id.imgdriverCall);

        dot2 = view.findViewById(R.id.dot2);
        dot1 = view.findViewById(R.id.dot1);

        line = view.findViewById(R.id.line);
        save = view.findViewById(R.id.save);
        mDemoSlider = (SliderLayout) view.findViewById(R.id.slider);

        origin = false;
        dest = false;
        sfmedium = Typeface.createFromAsset(getActivity().getAssets(), "sfmedium.ttf");
        sfregular = Typeface.createFromAsset(getActivity().getAssets(), "sfregular.ttf");
        edtDestination.setTypeface(sfmedium);
        edtOrigin.setTypeface(sfmedium);
        txtDriverStatus.setTypeface(sfmedium);
        txtVechileName.setTypeface(sfregular);
        txtVechileNo.setTypeface(sfmedium);
        txtDriverName.setTypeface(sfregular);
        preferences = getActivity().getSharedPreferences(Constants.preference, MODE_PRIVATE);
        editor = preferences.edit();
        driverid = common.getStringValue("driverid");
        bookingId = common.getStringValue("bookingId");

        imgdriverCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                driverNo=   txtDriverphoneNo.getText().toString().trim();
                if (!driverNo.equalsIgnoreCase("")) {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + driverNo));
                    startActivity(intent);
                } else {
                    common.showLongToast("Don't have number for calling.");
                }
            }
        });
        // Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        //toolbar.setVisibility(View.GONE);


//        worksans_regular = Typeface.createFromAsset(getActivity().getAssets(), "WorkSans-Regular.ttf");
        //    ubuntuB = Typeface.createFromAsset(getActivity().getAssets(), "Ubuntu-B.ttf");
        setOnRefreshListener = (SwipeRefreshLayout) view.findViewById(R.id.pullToRefresh);

        isLoggedIn = preferences.getBoolean(Constants.isLoggedIn, false);
        if (isLoggedIn) {
            setOnRefreshListener.setOnRefreshListener(this);
        } else {
        }
        edtOrigin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //   isLoggedIn = preferences.getBoolean(Constants.isLoggedIn, false);
                Intent intent = new Intent(getActivity(), PlacesActivity.class);
                startActivityForResult(intent, 100);
                origin = true;
                dest = false;
                /*if (isLoggedIn) {
                    Intent intent = new Intent(getActivity(), PlacesActivity.class);
                    startActivityForResult(intent, 100);
                    origin = true;
                    dest = false;
                } else {
                    common.showShortToast("Please Login/signUp first.");
                }*/

            }
        });

        edtDestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), PlacesActivity.class);
                startActivityForResult(intent, 100);
                origin = false;
                dest = true;

            }
        });
        txtDriverCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bookingId.equalsIgnoreCase("")) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Cancel Ride")
                            .setMessage("Are you sure you want to Cancel ride?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    cancelRide();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                } else {
                   common.showLongToast("Don't have any ride for Cancel.");
                }
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edtOrigin.getText().toString().isEmpty() && !edtDestination.getText().toString().isEmpty()) {
                    if (!originId.equalsIgnoreCase(destId)) {
                        Intent intent = new Intent(getActivity(), RideBookingPage2.class);
                        intent.putExtra("originId", originId);
                        intent.putExtra("destId", destId);
                        intent.putExtra("destinationPlace", destinationPlace);
                        intent.putExtra("originPlace", originPlace);
                        startActivityForResult(intent, 120);
                        // originId="";destId="";
                    } else {
                        common.showShortToast("Pickup location and Drop location are same ,Please Enter different location.");
                    }
                }
            }
        });
        BannerData data = new BannerData();
        data.setBannerImageUrl(R.drawable.banner_img);
        listBanner.add(data);

        BannerData data1 = new BannerData();
        data1.setBannerImageUrl(R.drawable.location_list_img_1);
        listBanner.add(data1);

        BannerData data2 = new BannerData();
        data2.setBannerImageUrl(R.drawable.location_list_img_2);
        listBanner.add(data2);

        BannerData data3 = new BannerData();
        data3.setBannerImageUrl(R.drawable.location_list_img_3);
        listBanner.add(data3);

        setBannerAfterListLoadedFromApi();
        return view;
    }

    private void setBannerAfterListLoadedFromApi() {

        HashMap<String, BannerData> url_maps = new HashMap<String, BannerData>();

        for (int k = 0; k < listBanner.size(); k++) {
            url_maps.put("banner" + k, listBanner.get(k));
        }


        for (String name : url_maps.keySet()) {
            DefaultSliderView textSliderView = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView.description(" ").image(url_maps.get(name)
                    .getBannerImageUrl())
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);
            textSliderView.bundle(new Bundle());
            //// textSliderView.getBundle().putString("extra", name);
            // textSliderView.getBundle().putString("urlELink", url_maps.get(name).getExternalLink());
            textSliderView.getBundle().putString("url", String.valueOf(url_maps.get(name).getBannerImageUrl()));
            // textSliderView.getBundle().putString("ID", url_maps.get(name).getID());
            mDemoSlider.addSlider(textSliderView);
        }
        if (url_maps.size() > 1) {
            mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
            mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            mDemoSlider.setDuration(5000);
            //    mDemoSlider.addOnPageChangeListener((ViewPagerEx.OnPageChangeListener) getActivity());

        } else {
            mDemoSlider.stopAutoCycle();
            mDemoSlider.setPagerTransformer(false, new BaseTransformer() {
                @Override
                protected void onTransform(View view, float v) {

                }
            });
        }
    }

    public void onSliderClick(BaseSliderView slider) {


        Object obj = new Object();

        Bundle bundle = slider.getBundle();


    }

    public void setOnSliderPageChangeListener(ViewPagerEx.OnPageChangeListener listener) {
        if (listener != null) {
            //  mViewPager.setOnPageChangeListener(listener);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                String id = data.getStringExtra("placeId");
                if (dest) {
                    edtDestination.setText("" + result);
                    destId = id;
                    destinationPlace = result;
                } else if (origin) {
                    edtOrigin.setText("" + result);
                    originId = id;
                    originPlace = result;
                }
                if (!originId.isEmpty() && !destId.isEmpty()) {
                    line.setVisibility(View.VISIBLE);
                    dot1.setVisibility(View.INVISIBLE);
                    dot2.setVisibility(View.INVISIBLE);
                } else {
                    line.setVisibility(View.GONE);
                    dot1.setVisibility(View.VISIBLE);
                    dot2.setVisibility(View.VISIBLE);
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    private void getData() {

        common.showSpinner(getActivity());

        String uri = Constants.BaseUrl + "updateuserinfo";
        try {
            JSONObject json = new JSONObject();
            json.put("OldPass", oldpassword.getText().toString());
            json.put("NewPass", newpassword.getText().toString());
            json.put("ID", common.getStringValue(Constants.id));


            JsonObjectRequest req = new JsonObjectRequest(uri, json,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                common.hideSpinner();
                                String s = response.toString();
                                String status = response.optString("Status");
                                String message = response.optString("Message");

                                if (status.equalsIgnoreCase("1")) {
                                    common.showShortToast(message);
                                    getActivity().onBackPressed();
                                } else {
                                    common.showShortToast("Wrong old password entered  try again");
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                common.hideSpinner();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            VolleyLog.d("error", error.getMessage());
                            common.hideSpinner();

                        }

                    }) {


                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    return headers;
                }
            };
            try {
                req.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(req);
            } catch (Exception e) {
                e.printStackTrace();
                common.hideSpinner();
            }
        } catch (Exception e) {
            e.printStackTrace();
            common.hideSpinner();

        }
    }

    private void userDetail() {
        String uri = Constants.BaseUrl + "driversidebooking.php";
        try {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, uri,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (setOnRefreshListener != null)
                                setOnRefreshListener.setRefreshing(false);

                            common.hideSpinner();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.optString("status");
                                String message = jsonObject.optString("message");

                                if (status.equalsIgnoreCase("1")) {

                                    parseData(jsonObject);

                                } else {
                                    rlStartlayout.setVisibility(View.GONE);
                                    common.removeKey("bookingId");
                                    common.removeKey("driverid");
                                    //  common.showShortToast(message);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (setOnRefreshListener != null)
                                    setOnRefreshListener.setRefreshing(false);
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (setOnRefreshListener != null)
                                setOnRefreshListener.setRefreshing(false);
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("driverid", driverid);

                    return params;
                }

            };

            try {
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);
            } catch (Exception e) {
                e.printStackTrace();
                common.hideSpinner();
            }
        } catch (Exception e) {
            e.printStackTrace();
            common.hideSpinner();

        }
    }

    private void parseData(JSONObject response) {
        try {
            //  JSONObject jsonObject = new JSONObject(response);
            JSONArray finalBookingjsonArray = response.getJSONArray("FinalBookingInfo");
            JSONObject jsonObject = finalBookingjsonArray.optJSONObject(0);
            JSONObject jsonFromLocation = jsonObject.optJSONObject("BookingFromLocation");
            JSONObject jsonToLocation = jsonObject.optJSONObject("BookingToLocation");
            JSONObject jsonBookingInfo = jsonObject.optJSONObject("BookingInfo");
            JSONObject jsonUserDetail = jsonObject.optJSONObject("UserDetail");
            JSONObject jsonDriverDetail = jsonObject.optJSONObject("DriverDetail");

            String userName = jsonDriverDetail.optString("name");
            driverNo = jsonDriverDetail.optString("mobile");
            String userPicUrl = jsonDriverDetail.optString("image");
            carname = jsonDriverDetail.optString("carname");
            carno = jsonDriverDetail.optString("carno");
            driverid = jsonBookingInfo.optString("driverid");
            bookingId = jsonBookingInfo.optString("id");
            txtDriverStatus.setText(userName + " will meet you to take your ride.");
            txtDriverName.setText(userName);
            txtDriverphoneNo.setText(driverNo);
            txtVechileNo.setText(carno);
            txtVechileName.setText(carname);
            if(!driverNo.equalsIgnoreCase("")){
                rlStartlayout.setVisibility(View.VISIBLE);
            }
            if (userPicUrl != null && !userPicUrl.equalsIgnoreCase("null"))
                Picasso.with(getActivity()).load(common.getStringValue(userPicUrl)).placeholder(R.drawable.profile_pic).into(imgDreiverProPic);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cancelRide() {

        common.showSpinner(getActivity());

        String uri = Constants.BaseUrl + "usercancelride.php";
        try {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, uri,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            String s = response.toString();
                            common.hideSpinner();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.optString("status");
                                String message = jsonObject.optString("message");

                                if (status.equalsIgnoreCase("1")) {
                                    /*edtname.setText("");
                                    edtphone.setText("");
                                    edtDestination.setText("");
                                    edtOrigin.setText("");*/
                                    // userDetail();
                                    bookingId = "";
                                    common.removeKey("driverid");
                                    common.removeKey("bookingId");
                                    rlStartlayout.setVisibility(View.GONE);

                                } else {
                                    common.showShortToast(message);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("bookingid", bookingId);
                    params.put("cancelby", "customer");
                    params.put("userid", common.getStringValue(Constants.id));

                    return params;
                }

            };

            try {
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);
            } catch (Exception e) {
                e.printStackTrace();
                common.hideSpinner();
            }
        } catch (Exception e) {
            e.printStackTrace();
            common.hideSpinner();

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (common.isNetworkAvailable())
            if (!bookingId.equalsIgnoreCase(""))
                userDetail();
            else {
                //   rlStartlayout.setVisibility(View.VISIBLE);
            }
        else {
            common.showLongToast("No internet connection");
            //setOnRefreshListener.setRefreshing(false);
        }
    }

    @Override
    public void onRefresh() {
        setOnRefreshListener.setRefreshing(true);
        //refreshList();
        if (common.isNetworkAvailable())
            userDetail();
        else {
            common.showLongToast("No internet connection");
            setOnRefreshListener.setRefreshing(false);
        }
    }
}
