package go.arunachal.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import go.arunachal.R;
import go.arunachal.activity.AddNewPlace;
import go.arunachal.activity.EditNameActivity;
import go.arunachal.activity.EditPasswordActivity;
import go.arunachal.activity.EditPhoneActivity;
import go.arunachal.helperclasses.Common;
import go.arunachal.helperclasses.Constants;
import go.arunachal.helperclasses.Util;
import go.arunachal.helperclasses.VolleySingleton;

/**
 * Created by pranav on 11/1/17.
 */

public class MyProfileFragment extends Activity {

    Common common;
    RequestQueue requestQueue;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final int REQUEST_CODE_Gallery = 0;
    private static final String IMAGE_DIRECTORY_NAME = "goarunachal";
    private static final String TAG = "UploadActivity";
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    static String timeStamp;
    EditText oldpassword, newpassword;
    TextView txtemail;
    EditText edtName, edtPhoneNo, edtPassword;
    TextInputLayout name_layout;
    Bitmap croppedBmp;
    private Bitmap bitmap;
    private TransferUtility transferUtility;
    private Uri fileUri;
    ImageView profilePic;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                android.os.Environment
                        .getExternalStoragePublicDirectory(android.os.Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        timeStamp = new SimpleDateFormat("HHmmss",
                Locale.getDefault()).format(new Date());
        String abc = timeStamp.toString() + ".jpg";
        //profilepic = Constants.AWSLINK + abc;
        File mediaFile;

        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        common = Common.getNewInstance(this);
        edtName = findViewById(R.id.edtName);
        edtPhoneNo = findViewById(R.id.edtPhoneNo);
        edtPassword = findViewById(R.id.edtPassword);
        txtemail = findViewById(R.id.txtemail);
        name_layout = findViewById(R.id.name_layout);
        profilePic = findViewById(R.id.profilePic);
        requestQueue = VolleySingleton.getInstance(this).getRequestQueue();
        preferences = getSharedPreferences(Constants.preference, MODE_PRIVATE);
        editor = preferences.edit();
        transferUtility = Util.getTransferUtility(MyProfileFragment.this);
        txtemail.setText(common.getStringValue(Constants.email));
        edtPhoneNo.setText(common.getStringValue(Constants.Phone));
        edtName.setText(common.getStringValue(Constants.FirstName));
        edtPassword.setText(common.getStringValue(Constants.Password));
        if (common.isNetworkAvailable()) {
            // profilePic
            if(!common.getStringValue(Constants.ProfilePic).trim().isEmpty()&&!common.getStringValue(Constants.ProfilePic).equalsIgnoreCase("null"))
           try {
               Picasso.with(this).load(common.getStringValue(Constants.ProfilePic)).placeholder(R.drawable.galleryplacholder).into(profilePic);
           }catch (Exception ex){

           }
        }
        Drawable dr = getResources().getDrawable(R.drawable.nextpage);
        Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
        Drawable d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 25, 40, true));

        //setCompoundDrawablesWithIntrinsicBounds (image to left, top, right, bottom)
        edtName.setCompoundDrawablesWithIntrinsicBounds(null, null, d, null);
        edtPhoneNo.setCompoundDrawablesWithIntrinsicBounds(null, null, d, null);
        edtPassword.setCompoundDrawablesWithIntrinsicBounds(null, null, d, null);
        name_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfileFragment.this, EditNameActivity.class);
                startActivityForResult(intent, 200);
            }
        });
        edtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfileFragment.this, EditNameActivity.class);
                startActivityForResult(intent, 200);
            }
        });
        edtPhoneNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfileFragment.this, EditPhoneActivity.class);
                startActivityForResult(intent, 300);
            }
        });
        edtPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfileFragment.this, EditPasswordActivity.class);
                startActivityForResult(intent, 400);
            }
        });
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertBoxForImagePic();
            }
        });
    }


    public void AlertBoxForImagePic() {


        new AlertDialog.Builder(MyProfileFragment.this)
                .setTitle("Please Wait....")
                .setMessage("Want to choose photo from")
                .setPositiveButton("Camera",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {


                                Intent intentcamera = new Intent(
                                        MediaStore.ACTION_IMAGE_CAPTURE);

                                fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

                                intentcamera.putExtra(MediaStore.EXTRA_OUTPUT,
                                        fileUri);

                                // start the image capture Intent
                                startActivityForResult(intentcamera,
                                        CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

                            }
                        })
                .setNegativeButton("Gallery",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {


                                Intent intent = new Intent();
                                intent.setType("image/*");
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                intent.addCategory(Intent.CATEGORY_OPENABLE);
                                startActivityForResult(intent,
                                        REQUEST_CODE_Gallery);

                            }
                        }).setIcon(android.R.drawable.ic_dialog_alert).show();

    }

    public Uri getOutputMediaFileUri(int type) {

        return Uri.fromFile(getOutputMediaFile(type));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_Gallery: {
                if (requestCode == REQUEST_CODE_Gallery
                        && resultCode == Activity.RESULT_OK)

                {
                    Uri uri = data.getData();

                    try {
                        String path = getPath(uri);
                        // String p=path.replace(path, timeStamp+".jpg");
                        beginUpload(path);

                    } catch (URISyntaxException e) {
                        Toast.makeText(MyProfileFragment.this,
                                "Unable to get the file from the given URI.  See error log for details",
                                Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "Unable to upload file from the given uri", e);
                    }
                    try {

                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 5;
                        bitmap = BitmapFactory.decodeFile(getPath(uri), options);
                        croppedBmp = Bitmap.createBitmap(bitmap, 0, 0,
                                bitmap.getWidth(), bitmap.getHeight() - 1);

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                }
                super.onActivityResult(requestCode, resultCode, data);
            }
            break;

            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE: {

                if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                    if (resultCode == Activity.RESULT_OK) {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 5;
                        if (fileUri != null && fileUri.getPath() != null) {
                            bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);
                            if (bitmap != null) {
                                croppedBmp = Bitmap.createBitmap(bitmap, 0, 0,
                                        bitmap.getWidth(), bitmap.getHeight() - 1);

                                String path = fileUri.getPath();
                                beginUpload(path);
                            } else {
                                common.showShortToast("Try again");
                            }
                        } else {
                            common.showShortToast("Try again");
                        }
                    }
                }
                break;
            }
            case 200: {
                if (resultCode == Activity.RESULT_OK) {
                    String result = data.getStringExtra("result");
                    if (!result.isEmpty())
                        edtName.setText(result);
                }

                break;
            }
            case 300: {
                if (resultCode == Activity.RESULT_OK) {
                    String result = data.getStringExtra("result");
                    if (!result.isEmpty())
                        edtPhoneNo.setText(result);
                }
                break;
            }
            case 400: {
                if (resultCode == Activity.RESULT_OK) {
                    String result = data.getStringExtra("result");

                }
            }
            default:
                break;
        }

    }

    public File saveBitmapToFile(File file) {
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 2;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE = 200;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, outputStream);

            return file;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    private void beginUpload(String filePath) {
        if (filePath == null) {
            Toast.makeText(MyProfileFragment.this, "Could not find the filepath of the selected file",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Uploading...");
        progressDialog.show();
        final File file = new File(filePath);
        File ff = saveBitmapToFile(file);
        String current_time = "";

        current_time = String.valueOf(System.currentTimeMillis());
        final String strPath = "addplaces/" + current_time + ".jpg";
        if (common.isNetworkAvailable()) {
            TransferObserver observer = transferUtility.upload(Constants.BUCKET_NAME, strPath,
                    ff);

            Constants.linkProfile = "https://s3.ap-south-1.amazonaws.com/goarunachalnew/" + strPath + "";
            observer.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {

                    if (state == TransferState.COMPLETED) {
                        progressDialog.dismiss();
                        String internalpath = file.getPath();
                        //  profilePic.setImageBitmap(croppedBmp);
                        // user_image.setImageBitmap(bitmap);
                        upData();

                    }

                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

                }

                @Override
                public void onError(int id, Exception ex) {
                    common.showLongToast("Try again");
                    progressDialog.dismiss();

                }
            });
            // imageFileList.add(ff);
        } else {
            progressDialog.dismiss();
            common.showShortToast("No internet");
        }

    }

    @SuppressLint("NewApi")
    private String getPath(Uri uri) throws URISyntaxException {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(MyProfileFragment.this, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return android.os.Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = MyProfileFragment.this.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    private void upData() {

        common.showSpinner(this);

        String uri = Constants.BaseUrl + "updateprofile.php";
        try {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, uri,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Toast.makeText(MainActivity.this,response,Toast.LENGTH_LONG).show();
                            String s = response.toString();
                            common.hideSpinner();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.optString("status");
                                String message = jsonObject.optString("message");

                                if (status.equalsIgnoreCase("1")) {
                                    profilePic.setImageBitmap(croppedBmp);
                                    JSONObject object = jsonObject.getJSONObject("UserDetail");
                                    common.showShortToast(message);
                                    editor.putString(Constants.Phone, object.optString("mobile"));
                                    editor.putString(Constants.email, object.optString("email"));
                                    editor.putString(Constants.FirstName, object.optString("name"));
                                    editor.putString(Constants.id, object.optString("id"));
                                    editor.putString(Constants.ProfilePic, object.optString("imgurl"));
                                    editor.putString(Constants.Password, object.optString("password"));
                                    editor.putBoolean(Constants.isLoggedIn, true);
                                    editor.commit();
                                    /*Intent returnIntent = new Intent();
                                    returnIntent.putExtra("result", edtName.getText().toString().trim());
                                    setResult(Activity.RESULT_OK, returnIntent);
                                    finish();*/


                                } else {
                                    common.showShortToast(message);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("imgurl", Constants.linkProfile);
                    params.put("id", common.getStringValue(Constants.id));

                    return params;
                }

            };

            try {
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);
            } catch (Exception e) {
                e.printStackTrace();
                common.hideSpinner();
            }
        } catch (Exception e) {
            e.printStackTrace();
            common.hideSpinner();

        }
    }
    private Drawable resize(Drawable image) {
        Bitmap b = ((BitmapDrawable) image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 50, 50, false);
        return new BitmapDrawable(getResources(), bitmapResized);
    }
}
