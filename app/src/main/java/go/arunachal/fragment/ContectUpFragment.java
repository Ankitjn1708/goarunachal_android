package go.arunachal.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import go.arunachal.R;
import go.arunachal.activity.PlacesActivity;
import go.arunachal.helperclasses.Common;
import go.arunachal.helperclasses.Constants;
import go.arunachal.helperclasses.VolleySingleton;

import static android.content.Context.MODE_PRIVATE;



public class ContectUpFragment extends Activity {

    View view;
    Common common;
    RequestQueue requestQueue;
    Typeface worksans_regular, ubuntuB;
    EditText oldpassword, newpassword, retypepassword;
    ImageView cancle, save;
    TextView textViewtitle,Noemail,address,txtVechileNo,contact,phone;
    EditText edtOrigin, edtDestination;
    boolean origin, dest;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    boolean isLoggedIn;
    Typeface sfmedium,sfregular;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contect_us_activity);
        sfmedium = Typeface.createFromAsset(this.getAssets(), "sfmedium.ttf");
        sfregular = Typeface.createFromAsset(this.getAssets(), "sfregular.ttf");
        textViewtitle=findViewById(R.id.textViewtitle);
        contact=findViewById(R.id.contact);
        address=findViewById(R.id.address);
        Noemail=findViewById(R.id.Noemail);
        phone=findViewById(R.id.phone);

        textViewtitle.setTypeface(sfmedium);

        contact.setTypeface(sfregular);
        address.setTypeface(sfregular);
        Noemail.setTypeface(sfregular);
        phone.setTypeface(sfregular);
        contact.setPaintFlags(contact.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        common = Common.getNewInstance(this);
        requestQueue = VolleySingleton.getInstance(this).getRequestQueue();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {

        }
    }

    private void getData() {

        common.showSpinner(this);

        String uri = Constants.BaseUrl + "updateuserinfo";
        try {
            JSONObject json = new JSONObject();
            json.put("OldPass", oldpassword.getText().toString());
            json.put("NewPass", newpassword.getText().toString());
            json.put("ID", common.getStringValue(Constants.id));


            JsonObjectRequest req = new JsonObjectRequest(uri, json,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                common.hideSpinner();
                                String s = response.toString();
                                String status = response.optString("Status");
                                String message = response.optString("Message");

                                if (status.equalsIgnoreCase("1")) {
                                    common.showShortToast(message);
                                  //  getActivity().onBackPressed();
                                } else {
                                    common.showShortToast("Wrong old password entered  try again");
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                common.hideSpinner();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            VolleyLog.d("error", error.getMessage());
                            common.hideSpinner();

                        }

                    }) {


                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    return headers;
                }
            };
            try {
                req.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(req);
            } catch (Exception e) {
                e.printStackTrace();
                common.hideSpinner();
            }
        } catch (Exception e) {
            e.printStackTrace();
            common.hideSpinner();

        }
    }

}
