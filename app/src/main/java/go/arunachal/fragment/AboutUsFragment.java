package go.arunachal.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import go.arunachal.R;
import go.arunachal.activity.PlacesActivity;
import go.arunachal.helperclasses.Common;
import go.arunachal.helperclasses.Constants;
import go.arunachal.helperclasses.VolleySingleton;
import go.arunachal.interfaces.DrawerLocker;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by pranav on 11/1/17.
 */

public class AboutUsFragment extends Activity {

    View view;
    Common common;
    RequestQueue requestQueue;
    Typeface worksans_regular, ubuntuB;
    EditText oldpassword, newpassword, retypepassword;
    ImageView cancle, save;
    TextView textViewtitle,txtDriverStatus,txtVechileName,txtVechileNo,txtDriverName;
    EditText edtOrigin, edtDestination;
    boolean origin, dest;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    boolean isLoggedIn;
    Typeface sfmedium,sfregular;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_aboutus);
        common = Common.getNewInstance(this);
        requestQueue = VolleySingleton.getInstance(this).getRequestQueue();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                if (dest)
                    edtDestination.setText("" + result);
                else if (origin)
                    edtOrigin.setText("" + result);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    private void getData() {

        common.showSpinner(this);

        String uri = Constants.BaseUrl + "updateuserinfo";
        try {
            JSONObject json = new JSONObject();
            json.put("OldPass", oldpassword.getText().toString());
            json.put("NewPass", newpassword.getText().toString());
            json.put("ID", common.getStringValue(Constants.id));


            JsonObjectRequest req = new JsonObjectRequest(uri, json,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                common.hideSpinner();
                                String s = response.toString();
                                String status = response.optString("Status");
                                String message = response.optString("Message");

                                if (status.equalsIgnoreCase("1")) {
                                    common.showShortToast(message);

                                } else {
                                    common.showShortToast("Wrong old password entered  try again");
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                common.hideSpinner();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            VolleyLog.d("error", error.getMessage());
                            common.hideSpinner();

                        }

                    }) {


                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    return headers;
                }
            };
            try {
                req.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(req);
            } catch (Exception e) {
                e.printStackTrace();
                common.hideSpinner();
            }
        } catch (Exception e) {
            e.printStackTrace();
            common.hideSpinner();

        }
    }

}
