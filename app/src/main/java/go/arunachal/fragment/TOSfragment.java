package go.arunachal.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import go.arunachal.R;
import go.arunachal.helperclasses.Common;
import go.arunachal.helperclasses.Constants;
import go.arunachal.helperclasses.VolleySingleton;

/**
 * Created by pranav on 11/1/17.
 */

public class TOSfragment extends Activity {

    View view;
    Common common;
    RequestQueue requestQueue;
    Typeface worksans_regular, ubuntuB;
    EditText oldpassword, newpassword, retypepassword;
    ImageView cancle, save;
    TextView texttitleValue, texttitlevalue2, texttitle2, texttitle, textViewtitle;
    EditText edtOrigin, edtDestination;
    boolean origin, dest;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    boolean isLoggedIn;
    Typeface sfmedium, sfregular;
    String termsService = "", termsPolicy = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.term_conditions_activity);
        sfmedium = Typeface.createFromAsset(this.getAssets(), "sfmedium.ttf");
        sfregular = Typeface.createFromAsset(this.getAssets(), "sfregular.ttf");

        texttitle = findViewById(R.id.texttitle);
        texttitle2 = findViewById(R.id.texttitle2);
        texttitlevalue2 = findViewById(R.id.texttitlevalue2);
        texttitleValue = findViewById(R.id.texttitleValue);
        textViewtitle = findViewById(R.id.textViewtitle);

        texttitle.setTypeface(sfmedium);
        texttitle2.setTypeface(sfmedium);
        textViewtitle.setTypeface(sfmedium);
        texttitlevalue2.setTypeface(sfregular);
        texttitleValue.setTypeface(sfregular);
        common = Common.getNewInstance(this);
        requestQueue = VolleySingleton.getInstance(this).getRequestQueue();
        getData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                if (dest)
                    edtDestination.setText("" + result);
                else if (origin)
                    edtOrigin.setText("" + result);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    private void getData() {


        common.showSpinner(this);

        String uri = Constants.BaseUrl + "privacy.php";
        try {

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, uri, new JSONObject(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                String s = response.toString();
                                parsedCatData(response);
                                common.hideSpinner();
                            } catch (Exception e) {
                                e.printStackTrace();
                                common.hideSpinner();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            VolleyLog.d("error", error.getMessage());
                            common.hideSpinner();

                        }

                    }) {


                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    return headers;
                }
            };
            try {
                req.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(req);
            } catch (Exception e) {
                e.printStackTrace();
                common.hideSpinner();
            }
        } catch (Exception e) {
            e.printStackTrace();
            common.hideSpinner();
        }
    }

    private void parsedCatData(JSONObject response) {
        try {

            JSONArray jsonArray = response.getJSONArray("PrivacyContent");
            JSONObject jsonObject = jsonArray.optJSONObject(0);
            termsService = jsonObject.optString("text");

            JSONObject jsonObject1 = jsonArray.optJSONObject(1);
            termsPolicy = jsonObject1.optString("text");

            texttitleValue.setText(termsService);
            texttitlevalue2.setText(termsPolicy);
        } catch (Exception e) {

        }
    }

}
