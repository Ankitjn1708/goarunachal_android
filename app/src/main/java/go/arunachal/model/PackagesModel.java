package go.arunachal.model;

/**
 * Created by rajesh on 25/10/18.
 */

public class PackagesModel {

    String id,packages,priceperkm,TotalCost,time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPackages() {
        return packages;
    }

    public void setPackages(String packages) {
        this.packages = packages;
    }

    public String getPriceperkm() {
        return priceperkm;
    }

    public void setPriceperkm(String priceperkm) {
        this.priceperkm = priceperkm;
    }

    public String getTotalCost() {
        return TotalCost;
    }

    public void setTotalCost(String totalCost) {
        TotalCost = totalCost;
    }
}
