package go.arunachal.model;

import android.os.Parcel;
import android.os.Parcelable;


public class BannerData implements Parcelable {

    public static final Creator CREATOR = new Creator() {
        public BannerData createFromParcel(Parcel in) {
            return new BannerData(in);
        }

        public BannerData[] newArray(int size) {
            return new BannerData[size];
        }
    };

    String ID;
    String RWAID;
    int BannerImageUrl;
    String BannerImageSmallUrl;
    String DateUploded;
    String StartDate;
    String EndDate;
    String ExternalLink;
    String Addedby;
    String IsActive;
    String BannerType;
    String RefID;

    public BannerData(){}

    public BannerData(Parcel in){
        this.ID = in.readString();
        this.RWAID = in.readString();
        this.BannerImageUrl = in.readInt();
        this.BannerImageSmallUrl = in.readString();
        this.DateUploded = in.readString();
        this.StartDate = in.readString();
        this.EndDate = in.readString();
        this.ExternalLink = in.readString();
        this.Addedby = in.readString();
        this.IsActive = in.readString();
        this.BannerType = in.readString();
        this.RefID = in.readString();
    }

    public String getBannerType() {
        return BannerType;
    }

    public void setBannerType(String bannerType) {
        BannerType = bannerType;
    }

    public String getRefID() {
        return RefID;
    }

    public void setRefID(String refID) {
        RefID = refID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getRWAID() {
        return RWAID;
    }

    public void setRWAID(String RWAID) {
        this.RWAID = RWAID;
    }

    public int getBannerImageUrl() {
        return BannerImageUrl;
    }

    public void setBannerImageUrl(int bannerImageUrl) {
        BannerImageUrl = bannerImageUrl;
    }

    public String getBannerImageSmallUrl() {
        return BannerImageSmallUrl;
    }

    public void setBannerImageSmallUrl(String bannerImageSmallUrl) {
        BannerImageSmallUrl = bannerImageSmallUrl;
    }

    public String getDateUploded() {
        return DateUploded;
    }

    public void setDateUploded(String dateUploded) {
        DateUploded = dateUploded;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getExternalLink() {
        return ExternalLink;
    }

    public void setExternalLink(String externalLink) {
        ExternalLink = externalLink;
    }

    public String getAddedby() {
        return Addedby;
    }

    public void setAddedby(String addedby) {
        Addedby = addedby;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ID);
        dest.writeString(this.RWAID);
        dest.writeInt(this.BannerImageUrl);
        dest.writeString(this.BannerImageSmallUrl);
        dest.writeString(this.DateUploded);
        dest.writeString(this.StartDate);
        dest.writeString(this.EndDate);
        dest.writeString(this.ExternalLink);
        dest.writeString(this.Addedby);
        dest.writeString(this.IsActive);
        dest.writeString(this.BannerType);
        dest.writeString(this.RefID);
    }

}
