package go.arunachal.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import go.arunachal.R;
import go.arunachal.model.NavDrawerItem;

//import com.example.elitebook8740p.pynt_sanganan.R;


public class NavDrawerListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<NavDrawerItem> navDrawerItems;
    TextView txtTitle;
    ImageView imgIcon;
    Typeface sfmedium;


    public NavDrawerListAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItems) {
        this.context = context;
        this.navDrawerItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            sfmedium = Typeface.createFromAsset(context.getAssets(), "sfmedium.ttf");
            convertView = mInflater.inflate(R.layout.drawer_list_item, null);

            txtTitle = convertView.findViewById(R.id.title);
            txtTitle.setTypeface(sfmedium);
            imgIcon = convertView.findViewById(R.id.navImage);
        }


        //imgIcon.setImageResource(navDrawerItems.get(position).getImage());
        Picasso.with(context).load(navDrawerItems.get(position).getImage()).into(imgIcon);
        txtTitle.setText(navDrawerItems.get(position).getTitle());

        return convertView;
    }




}
